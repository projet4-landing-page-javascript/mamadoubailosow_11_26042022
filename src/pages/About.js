import React from "react";
import bannerAbout from '../assets/images/bannerMountain.png';
import Banner from "../components/Banner";
import DropDownMenu from "../components/DropDownMenu";
import Footer from "../components/Footer";
import Header from "../components/Header";
import { dataAbout } from "../datas/dataAbout";

const About = () => {

  return (
    <React.Fragment>
      <Header />
      <Banner image={bannerAbout} />
      <section className="aboutContainerDropdown">
        <DropDownMenu dropdown={dataAbout.fiability} nameTitle='Fiabilité' widthSize='' />
        <DropDownMenu dropdown={dataAbout.respect} nameTitle='Respect' widthSize='' />
        <DropDownMenu dropdown={dataAbout.servie} nameTitle='Service' widthSize='' />
        <DropDownMenu dropdown={dataAbout.security} nameTitle='Sécurité' widthSize='' />
      </section>
      <Footer />
    </React.Fragment>
  );
};

export default About;
