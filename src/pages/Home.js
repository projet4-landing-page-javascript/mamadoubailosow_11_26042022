import React from 'react';
import homeBanner from '../assets/images/visual.png';
import Banner from '../components/Banner';
import Footer from '../components/Footer';
import Galerry from '../components/Galerry';
import Header from '../components/Header';

const Home = () => {
    const text = "Chez vous, partout et ailleurs"
    return (
        <React.Fragment>
            <Header />
            <Banner text={text} image={homeBanner} />
            <Galerry />
            <Footer />
        </React.Fragment>
    )
};

export default Home;