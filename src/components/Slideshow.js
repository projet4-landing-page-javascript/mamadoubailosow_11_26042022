import { useState } from "react";
import { FaAngleLeft, FaAngleRight } from "react-icons/fa";

const Slideshow = ({ housing }) => {
  const length = housing.pictures.length;
  const [currentMedia, setCurrentMedia] = useState(0);

  const nextSlide = () => {
    setCurrentMedia(currentMedia === length - 1 ? 0 : currentMedia + 1);
  };


  const prevSlide = () => {
    setCurrentMedia(currentMedia === 0 ? length - 1 : currentMedia - 1);
  };

  return (
    <section className="container-slide">
      <FaAngleRight
        className="container-slide__iconright"
        onClick={nextSlide}
      />
      <FaAngleLeft className="container-slide__iconleft" onClick={prevSlide} />
      {housing.pictures.map((media, index) =>
        index === currentMedia ? (
          <img
            src={media}
            alt={`index${index}`}
            key={index}
            className="container-slide__media"
          />
        ) : null
      )}
      <p className="container-slide__number">{`${currentMedia + 1
        }/${length}`}</p>
    </section>
  );
};

export default Slideshow;
