import React, { useState } from "react";
import { FaChevronDown } from "react-icons/fa";




const DropDownMenu = ({ dropdown, dropDownType, nameTitle, widthSize, move }) => {
    const [open, setOpen] = useState(false);

    const handleClick = () => {
        setOpen(!open);
    };

    // condition pour la liste ou la description
    const result =
        dropDownType === "array" ? (
            dropdown.map((equipement, index) => (
                <li key={index} className="dropdown__toggle--items">
                    {equipement}
                </li>
            ))
        ) : (
            <li className="dropdown__toggle--items">{dropdown}</li>
        );



    return (
        <aside className='dropdown'>
            <button
                type="button"
                className="dropdown__title"
                onClick={handleClick}
            >{
                    nameTitle
                }</button>
            <FaChevronDown className={`dropdown__icon ${open ? "rotation" : null}`} />
            <ul className='dropdown__toggle' style={open ? { padding: '10px 15px' } : null}>
                {open ? result : null}
            </ul>
        </aside>
    );
};

export default DropDownMenu;
