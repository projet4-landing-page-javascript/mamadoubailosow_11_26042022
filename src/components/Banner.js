import PropTypes from 'prop-types';
import React from 'react';

const Banner = ({ text, image }) => {
    return (
        <div className="banner" style={{ background: `url(${image}) no-repeat center` }}>
            <p className='banner__title'>{text}</p>
        </div >
    )
}

Banner.propTypes = {
    image: PropTypes.string.isRequired
}

export default Banner;