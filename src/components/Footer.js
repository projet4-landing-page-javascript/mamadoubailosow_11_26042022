import React from 'react';
import whitelogo from '../assets/images/WhiteLOGO.png'
const Footer = () => {
    return (
        <footer className='footer'>
            <div className='footer__logo'>
                <img  src = {whitelogo}  alt ="footer_logo" className='footer__logo--whitelogo'></img>
            </div>
            <p className='footer__right'> ©2022 Kasa. All rights reserved</p>            
        </footer>
    );
};

export default Footer;